# The `psquared_consume` Project #

The `psquared_consume` project contains `pp-consume` and `pp-push` executable along with the classes necessary to specialize the [`rabbitmq-consume`](https://pypi.org/project/rabbitmq-consume/) project to consume [`psquared`](https://gitlab.com/nest.lbl.gov/psquared) messages and process them.
